/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.robotpro;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        TableMap map = new TableMap(10,10);
        Robot robot = new Robot(2,2,'R',map);
        Bomb bomb = new Bomb (5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        
        while(true) {
            map.showMap();
            char direction = InputDirection(sc);
            if(direction == 'q') {
                printBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printBye() {
        System.out.println("Bye");
    }

    private static char InputDirection(Scanner sc) {
        String str = sc.next();
        char direction = str.charAt(0);
        return direction;
    }
}
